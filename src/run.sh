# "cargo build --release" need to be run beforehand
#! /bin/bash

cd "$(dirname "$0")"

mkdir -p log
NOW=$(date +"%Y-%m-%d")
LOGFILE="$NOW.log"
OUTPUT=log/$LOGFILE

./manga_scrapper > $OUTPUT
