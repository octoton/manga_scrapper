use scraper::{Html, Selector};

use std::fs::{File, OpenOptions};
use std::io::prelude::*;

use titlecase::titlecase;

use lettre::transport::smtp::authentication::Credentials;
use lettre::{Message, SmtpTransport, Transport};

use std::fmt::{Display, Formatter, Error};
struct NewContent(Vec<(String, String, usize)>);

impl Display for NewContent {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        let mut str = String::new();

        for elem in &self.0[0..self.0.len()] {
            str.push_str(&titlecase(&elem.0.to_string()));
            str.push_str(" Volume ");
            str.push_str(&elem.2.to_string());
            str.push_str(" (name: ");
            str.push_str(&elem.1.to_string());
            str.push_str(")\n");
        }

        write!(f, "{}", str)
    }
}

impl Default for NewContent {
	#[inline]
	fn default() -> NewContent {
		NewContent {
			0: Vec::new()
		}
	}
}

impl NewContent {
	// Public function
	/// Create new resolver with all value to default
	pub fn new() -> NewContent {
		NewContent {
			..Default::default()
		}
	}
}

fn main() {
	let filename: String = "config.json".to_owned();
	let mut file = OpenOptions::new()
		.read(true)
		.open(&filename).expect("Need to create config.json file, look at src json example");

	let mut contents = String::new();
	file.read_to_string(&mut contents).unwrap();
	let config = json::parse(&contents).unwrap();

	let home_adr: String = config["website"]["home"].to_string();
	let search_adr: String = config["website"]["search_address"].to_string();
	let link_are_relative: bool = config["website"]["link_are_relative"].as_bool().unwrap();
	let separator: String = config["website"]["separator"].to_string();
	let retry = config["website"]["retry"].as_usize().unwrap();

	let filename: String = "manga.json".to_owned();
	let mut file = OpenOptions::new()
		.read(true)
		.open(&filename).expect("Need to create manga.json file, look at src json example");

	let mut contents = String::new();
	file.read_to_string(&mut contents).unwrap();
	let mut parsed = json::parse(&contents).unwrap();

	let mut new_manga: NewContent = NewContent::new();

	//TODO: don't need to now the name of each elements to go throug them, just use a clean json array in the file
	let elements = ["mangas", "bds"];
	for k in 0..elements.len() {
		let element = elements[k];
		for i in 0..parsed[element].len() {
			if parsed[element][i]["monitored"].as_bool().unwrap() {
				let title = parsed[element][i]["title"].to_string();
				let j = 0;
				let mut new_volume_number = parsed[element][i]["volumes"][j]["number"].as_usize().unwrap();

				loop {
					new_volume_number += 1;

					let mut adr: String = String::new();
					if link_are_relative {
						adr.push_str(&home_adr.clone());
					}
					adr.push_str(&search_adr);
					adr.push_str(&title.replace(" ", &separator));
					adr.push_str("+");
					
					let special_number_separator = &parsed[element][i]["volumes"][j]["special number separator"];
					if !special_number_separator.is_null() {
						adr.push_str(&special_number_separator.to_string());
					}
					adr.push_str(&new_volume_number.to_string());
					println!("");

					println!("wanted: {} n°{}", title, new_volume_number);
					print!("  result: ");
					match search_new_infos(&adr, &home_adr, link_are_relative, retry) {
						None => { println!("No new chapter found for {}", title); break; },
						Some(result) => {
							println!("{}", result.title);
							new_manga.0.push((title.clone(), result.title.clone(), new_volume_number));

							let mut new_manga = json::object::Object::new();
							new_manga.insert("title", parsed[element][i]["volumes"][j]["title"].clone().into());
							new_manga.insert("number", parsed[element][i]["volumes"][j]["number"].clone().into());
							new_manga.insert("buy", parsed[element][i]["volumes"][j]["buy"].clone().into());
							new_manga.insert("cover", parsed[element][i]["volumes"][j]["cover"].clone().into());
							parsed[element][i]["volumes"].push(new_manga).unwrap();

							parsed[element][i]["volumes"][j]["title"] = result.title.into();
							parsed[element][i]["volumes"][j]["number"] = new_volume_number.into();
							parsed[element][i]["volumes"][j]["buy"] = false.into();
							parsed[element][i]["volumes"][j]["cover"] = result.cover.into();
						}
					};
				}
			}
		}
	}

	if new_manga.0.len() > 0 {
		let mail_info = MailInfo {
			from_address: config["mail"]["from"].to_string(),
			to_address: config["mail"]["to"].to_string(),
			smtp_server: config["mail"]["smtp"].to_string(),
			smtp_username: config["mail"]["identifier"].to_string(), 
			smtp_password: config["mail"]["password"].to_string()
		};
		
		println!("\n{}", new_manga);

		send_mail(new_manga, mail_info);

		let mut file = File::create(&filename).expect("Unable to write file");
		write!(file, "{:#}", parsed).expect("Unable to write file");
	}
}

#[derive(Debug)]
struct MailInfo {
    from_address: String,
    to_address: String,
    smtp_server: String,
    smtp_username: String,
    smtp_password: String
}

fn send_mail(new_book: NewContent, email_info: MailInfo) {
	let content: String = new_book.to_string();
	println!("{}", content);

	let email = Message::builder()
		.from(email_info.from_address.parse().unwrap())
		.to(email_info.to_address.parse().unwrap())
		.subject("New manga")
		.body(content)
		.unwrap();

	let creds = Credentials::new(email_info.smtp_username.to_string(), email_info.smtp_password.to_string());

	// Open a remote connection to gmail
	let mailer = SmtpTransport::relay(&email_info.smtp_server)
		.unwrap()
		.credentials(creds)
		.build();

	// Send the email
	match mailer.send(&email) {
		Ok(_) => println!("Email sent successfully!"),
		Err(e) => panic!("Could not send email: {:?}", e),
	}
}

struct SearchResults {
    title: String,
    cover: String
}

fn search_new_infos(url: &str, home: &str, link_are_relative: bool, retry: usize) -> Option<SearchResults> {

	let mut number_of_try = 0;
	let request = loop {
		let result = reqwest::blocking::get(url);
		match result {
			Ok(data) => break data,
			Err(_err) => {
				number_of_try += 1;
				if number_of_try == retry {
					return None;
				}
			}
		};
	};

	let body = request.text().unwrap();

	// parses string of HTML as a document
	let fragment = Html::parse_document(&body);

	// parses based on a CSS selector
	let titles = Selector::parse(".product-title").unwrap();
	let articles = Selector::parse(".thumbnail-container").unwrap();
	let links = Selector::parse(".product-thumbnail").unwrap();
	let to_come_up = Selector::parse(".text-danger").unwrap();
	//let covers = Selector::parse("img").unwrap();
	let covers = Selector::parse(".js-qv-product-cover").unwrap();

	// iterate over elements matching our selector
	for article in fragment.select(&articles) {
		// grab the headline text and place into a vector
		let title = article.select(&titles).next().unwrap();
		let title: String = title.value().attr("title").unwrap().to_string();

		// get link to check if article existing or only will be
		let link_obj = article.select(&links).next().unwrap();

                //let cover = link_obj.select(&covers).next().unwrap();
                //let cover: String = cover.value().attr("src").unwrap().to_string();

		let mut link: String = String::new();
		if link_are_relative {
			link.push_str(home);
		}
		link.push_str(&link_obj.value().attr("href").unwrap().to_string());
		print!("link: {}", link);
		let result = reqwest::blocking::get(&link).unwrap();
		let body = result.text().unwrap();
		let elements = Html::parse_document(&body);

		let cover = elements.select(&covers).next().unwrap();
		let cover: String = cover.value().attr("src").unwrap().to_string();

		print!(" -- ");
		for _to_soon in elements.select(&to_come_up) {
			println!("To soon: this volume is not yet available for sale");
			return None;
		}

		return Some(SearchResults {title: title, cover: cover});
	}

	return None;
}
